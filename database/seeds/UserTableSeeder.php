<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'name' => 'Ali Iskandar',
            'email' => 'user@gmail.com',
            'phone' => '019-7896543',
            'password' => bcrypt('pass1234'),
            'status' => 'active',
            'role' => 'user',
            'access_power' => 100,
            'remark' => 'I am a user',
        ]);

        User::create([
            'name' => 'Fatimah Ahmad',
            'email' => 'admin@gmail.com',
            'phone' => '012-7685434',
            'password' => bcrypt('pass1234'),
            'status' => 'active',
            'role' => 'admin',
            'access_power' => 200,
            'remark' => 'I am a merchant',
        ]);

        User::create([
            'name' => 'Faeza Seri',
            'email' => 'super_admin@gmail.com',
            'phone' => '012-3455543',
            'password' => bcrypt('pass1234'),
            'status' => 'active',
            'role' => 'super_admin',
            'access_power' => 1000,
            'remark' => 'I am a super administrator',
        ]);

        User::create([
            'name' => 'Ahmad Hasif',
            'email' => 'dev@gmail.com',
            'phone' => '012-77885432',
            'password' => bcrypt('pass1234'),
            'status' => 'active',
            'role' => 'super_admin',
            'access_power' => 1000,
            'remark' => 'I am the developer',
        ]);

        User::create([
            'name' => 'Arham Zulqarnaen',
            'email' => 'arhamzul@gmail.com',
            'phone' => '012-5673222',
            'password' => bcrypt('pass1234'),
            'role' => 'super_admin',
            'access_power' => 1000,
            'status' => 'active',
            'remark' => 'I am the developer',
        ]);
    }
}
