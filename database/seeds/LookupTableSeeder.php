<?php

use Illuminate\Database\Seeder;
use App\Lookup;

class LookupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear table everytime seed table ni
        DB::table('lookups')->delete();

        // user status
        Lookup::create([
            'name'          => 'user_status',
            'key'           => 'active',
            'value'         => 'Active'
        ]);

        Lookup::create([
            'name'          => 'user_status',
            'key'           => 'inactive',
            'value'         => 'Inactive'
        ]);

        Lookup::create([
            'name'          => 'user_role',
            'key'           => 'user',
            'value'         => 'User'
        ]);

        Lookup::create([
            'name'          => 'user_role',
            'key'           => 'admin',
            'value'         => 'Admin'
        ]);

        Lookup::create([
            'name'          => 'user_role',
            'key'           => 'super_admin',
            'value'         => 'Super Admin'
        ]);
    }
}
