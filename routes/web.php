<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// For Public Access
Route::get('/', 'WebController@index');
Route::get('about-us', 'WebController@aboutUs');
Route::get('logout', 'Auth\LoginController@logout');

// API
//Route::get('api/user','UserController@getAllUsers');

// All activities monitored by audit trail
// ---- FOR USER (AND ABOVE) ----
Route::group(['middleware' => ['role:user','audit']], function () {
    Route::get('home', 'HomeController@index');
    // Setting
    Route::get('setting/language/{lang}', 'SettingController@language');
    // User
    Route::get('user/change-password', 'UserController@changePassword');
    Route::post('user/change-password', 'UserController@updatePassword');
    Route::get('user/profile', 'UserController@profile');
    // Medal Tally
    Route::get('medal', 'MedalController@index');
});

// FOR ADMIN ----
Route::group(['middleware' => ['role:admin','audit']], function () {
    // MEMO
    Route::get('memo', 'MemoController@index');
    Route::get('memo/create', 'MemoController@create');
    Route::post('memo/create', 'MemoController@store');
    Route::get('memo/edit/{id}', 'MemoController@edit');
    Route::post('memo/edit/{id}', 'MemoController@update');
    Route::get('memo/show/{id}', 'MemoController@show');
    Route::get('v/destroy/{id}', 'MemoController@destroy');

});

// ---- FOR SUPER ADMIN (CTM) ----
Route::group(['middleware' => ['role:super_admin','audit']], function () {
    // User Management
    Route::get('user', 'UserController@index');
    Route::get('user/create', 'UserController@create');
    Route::post('user/create', 'UserController@store');
    Route::get('user/show/{id}', 'UserController@show');
    Route::get('user/edit/{id}', 'UserController@edit');
    Route::post('user/edit/{id}', 'UserController@update');
    Route::get('user/destroy/{id}', 'UserController@destroy');
    Route::get('user/reset-password/{id}', 'UserController@resetPassword');
    Route::get('user/update-profile', 'UserController@editProfile');
    Route::post('user/update-profile', 'UserController@updateProfile');

    // Audit Trail
    Route::get('audit', 'AuditController@index');
    Route::get('audit/mark/{id}/{flag}', 'AuditController@mark');
});
