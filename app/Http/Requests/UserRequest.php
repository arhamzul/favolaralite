<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rule = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'min:10',
            'avatar' => 'file',
            'password' => 'required|min:8|confirmed',
            'role' => 'required',
            'status' => 'required',
//            'remark' => 'min:3',
        ];

       return $rule;

    }

    public function messages()
    {
        return [
            'remark.min' => 'Alang-alang nak tulis remark tu, tulis la panjang2',
        ];
    }
}
