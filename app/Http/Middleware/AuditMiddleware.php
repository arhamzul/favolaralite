<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Audit;
use Request;

class AuditMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        echo 'this is audit';

        if (Auth::check()) {
            $audit = new Audit();
            $audit->user_id = Auth::user()->id;
            $audit->ip = Request::ip();
            $audit->url = $request->url();
            $audit->note = '-';
            $audit->save();
        } else {
            return redirect('login');
        }
        return $next($request);
    }
}
