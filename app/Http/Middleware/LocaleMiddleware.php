<?php

namespace App\Http\Middleware;
use Session;
use App;
use Closure;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = Session::get('preferedLocale');

        if ($lang == null) {
            session(['preferedLocale' => 'ms']);
        }

        App::setLocale($lang);

        return $next($request);
    }
}
