<?php

namespace App\Http\Middleware;

use App\Page;
use App\User;
use Closure;
use Auth;
use Debugbar;
class AclMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path = $request->path();
        $page = Page::where('path', $path)->first();
        $user = Auth::user();

        // ******* Start Dev Mode (Comment out this section in production)
        // For Development Only :: To Allow Dev to Always Able To Access Newly Created Page Witout the Need to Add page list in ACL Table
        if (User::getUserHighestRole(auth()->user()->id) == 'developer') {

//            DebugBar::wa('Dev. accessing ' . $path);
//            DebugBar::warning('Developer accessing ' . $path);
            Debugbar::warning('Dev. access page :: ' . $path);
            return $next($request);
        }
        // *******  End dev mode

        // check sama ada role yang user tu ada, boleh tak access page yang dia nak access.
        // get role yang diperlukan untuk access path yang dia nak access
        $userRoles = $user->role()->get();
        $accessFlag = false;
        foreach ($userRoles as $userRole) {
            $canAccess = Page::canAccess($userRole->id, $page->id);
            if ($canAccess) {
                $accessFlag = true;
            }
        }

        if ($canAccess) {
            return $next($request);
        } else {
            return redirect('logout');
        }
    }
}
