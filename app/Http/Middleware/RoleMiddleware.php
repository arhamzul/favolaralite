<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $routeRole)
    {
// If the user is not login, redirect to homepage (which is login page)
        // Since role if pyramid (super admin can access what admin can and so on)

        if (Auth::guest()) {
            return Redirect::to('/login');
        } else {
            // If a normal user can access it, the other higher ranking role can access it as well (inverted pyramid)
            $userRole = Auth::user()->role;         // role yang diambik dari table user, utk user ni

            if ($routeRole == 'user') {
                if ($userRole == 'user' || $userRole == 'admin' || $userRole == 'super_admin') {
                    return $next($request);
                }
            }

            if ($routeRole == 'admin') {
                if ($userRole == 'admin' || $userRole == 'super_admin') {
                    return $next($request);
                }
            }

            if ($routeRole == 'super_admin') {
                if ($userRole == 'super_admin') {
                    return $next($request);
                }
            }

            echo "routeRole : $routeRole";
            echo "<br>";
            echo "userRole : $userRole";
            dd('If you see this message, check RoleMiddleware. Something must goes wrong');

        }
    }
}
