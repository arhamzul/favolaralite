<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
//        return view('web.index');
        return redirect('login');
    }

    public function aboutUs()
    {
        return view('web.about_us');
    }
}
