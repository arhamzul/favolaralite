<?php

namespace App\Http\Controllers\Api;

use App\Memo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemoController extends Controller
{
    public function getLatest($limit)
    {
        $memos = Memo::orderBy('id', 'desc')->take($limit)->get();

        $myData = [
            'memos' => $memos
        ];

        return response($myData, 200);
    }
}
