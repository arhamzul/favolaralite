<?php

namespace App\Http\Controllers;

use App\Audit;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Redirect;


class AuditController extends Controller
{
    public function index() {
        $audits = Audit::with('user')->limit(10000)->get();
//        $audits = Audit::limit(10000)->get();
        return view('audit.index', compact('audits', 'hide'));
    }

    public function mark($id, $flag)
    {
        $audit = Audit::find($id);
        $audit->flag = $flag;
        if ($audit->note == '-') {
            $audit->note = '';
        }
        $audit->note .= "<i class='icon-add' style='margin: 1em'></i> Mark as $flag by " . Auth::user()->name . " on " . Carbon::now()->toFormattedDateString() . '.<br>';
        $audit->save();

        return Redirect::to('audit')->with('successMessage', "The audit log has been successfully mark as $flag" );
    }
}


