<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function language($lang)
    {
        if ($lang == 'english') {
            session(['preferedLocale' => 'en']);
        } else {
            session(['preferedLocale' => 'ms']);
        }



        return back();
    }
}
