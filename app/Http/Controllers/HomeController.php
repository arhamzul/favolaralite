<?php

namespace App\Http\Controllers;

use App\Memo;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $memos = Memo::orderBy('id', 'desc')->get();
        return view('home.index', compact('memos'));
    }
}
