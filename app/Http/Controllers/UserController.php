<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserRequestEdit;
use App\Lookup;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Auth;
use Hash;
use Redirect;
use Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $roles = Lookup::where('name', '=', 'user_role')->get();
        $roles = Lookup::where('name', 'user_role')->get();
        $statuses = Lookup::where('name', '=', 'user_status')->get();

        return view('user.create', compact('roles', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if ($request->role == 'user') {
            $accessPower = 100;
        } elseif ($request->role == 'admin') {
            $accessPower = 200;
        } elseif ($request->role == 'super_admin') {
            $accessPower = 1000;
        } else {
            $accessPower = 0;
        }

        $user = new User();
        $user->name = request('name');
        $user->email = request('email');
        $user->phone = request('phone');
        // Untuk upload gambar
        if (isset($request->avatar)) {
            if ($request->file('avatar')->isValid()) {
                $destinationPath = "images/avatars/";
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('avatar')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(500, 500)->save();

                $user->avatar = '/' . $destinationPath . $fileName;
            }
        }

        $user->password = bcrypt(request('password'));
        $user->role = request('role');
        $user->access_power = $accessPower;
        // user:100, merchant:200, admin:1000
        $user->status = request('status');
        $user->remark = request('remark');
        $user->save();

        // lepas user dah simpan dlm DB, kita nak redirect ke senarai users
        return redirect('user');        // page yg paparkan senarai user
                                        //iniaga.dev/user
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('user.show', compact('user'));
    }

    // Profile
    public function profile()
    {
        $user = Auth::user();
        return view('user.show', compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);        // Select * from users where 'id' = $id
        $roles = Lookup::where('name', 'user_role')->get();
        $statuses = Lookup::where('name', '=', 'user_status')->get();

        return view('user.edit', compact('user', 'roles', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequestEdit $request, $id)
    {
        if ($request->role == 'user') {
            $accessPower = 100;
        } elseif ($request->role == 'admin') {
            $accessPower = 200;
        } elseif ($request->role == 'super_admin') {
            $accessPower = 1000;
        } else {
            $accessPower = 0;
        }

        $user = User::findOrFail($id);      // <---- tukar yg ni je
        $user->name = request('name');
        $user->email = request('email');
        $user->phone = request('phone');
        // Untuk upload gambar
        if (isset($request->avatar)) {
            if ($request->file('avatar')->isValid()) {
                $destinationPath = "images/avatars/";
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('avatar')->move($destinationPath, $fileName);

                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(500, 500)->save();

                $user->avatar = '/' . $destinationPath . $fileName;
            }
        }

        // check password ada value ke tak.
        // kalau tak de, jangan update dengan DB
        if (isset($request->password)){
            $user->password = bcrypt(request('password'));
        }

        $user->role = request('role');
        $user->access_power = $accessPower;
        // user:100, merchant:200, admin:1000
        $user->status = request('status');
        $user->remark = request('remark');
        $user->save();

        // lepas user dah simpan dlm DB, kita nak redirect ke senarai users
        return redirect('user');        // page yg paparkan senarai user
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return back();
    }

    public function changePassword()
    {
        return view('user.change_password');
    }

    public function updatePassword(ChangePasswordRequest $request)
    {
        $user = Auth::user();

        // make sure old password is genuine
//        $genuine = Hash::check($request->old_password, $user->password);
        $genuine = Hash::check(request('old_password'), $user->password);
        if ($genuine == false) {
//            return Redirect::back()->withInput()->with('errorMessage', 'Wrong old password.');
            return back()->withInput()->with('errorMessage', 'Wrong old password.');
        }

        $user->password = bcrypt(request('old_password'));

        try {
            $user->save();
            return back()->with('successMessage', 'Password has been changed successfully.');
        } catch (\Exception $e) {
            return back()->withInput()->with('errorMessage', 'Unable to change password.');
        }
    }

    public function resetPassword($id)
    {
        $user = User::findOrFail($id);
        // generate 8 character randomly
        $randomPassword = str_random(8);
        // change user's password with the newly generated 8 char
        $user->password = bcrypt($randomPassword);

        // send email to user
        $data['new_password']  = $randomPassword;
        $data['name']  = $user->name;
        $data['email']  = $user->email;

        Mail::send('emails.reset_password', $data, function($message) use ($data)
        {
            $message->from('admin@iniaga.com.my', "Admin iNiaga");
            $message->subject("New Password - iNiaga System");
            $message->to($data['email']);
        });

        try {
            $user->save();
            return Redirect::to('user')->with('successMessage', "$user->name's password has been reset and sent to $user->email. The email may takes as long as 5 minutes to arrive.");
        } catch (\Exception $e) {
            return back()->withInput()->with('errorMessage', 'Unable to reset password.');
        }
    }

    public function editProfile()
    {
        $user = Auth::user();
        return view('user.edit_profile', compact('user'));
    }

    public function updateProfile()
    {
        $user = Auth::user();
        $user->name = request('name');
        $user->email = request('email');
        $user->phone = request('phone');
        // Untuk upload gambar
        if (isset($request->avatar)) {
            if ($request->file('avatar')->isValid()) {
                $destinationPath = "images/avatars/";
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $fileName = str_random(32) . '.' . $extension;
                $request->file('avatar')->move($destinationPath, $fileName);
                // standardize the image dimension (optional)
                Image::make($destinationPath.$fileName)->fit(500, 500)->save();
                $user->avatar = '/' . $destinationPath . $fileName;
            }
        }

        try {
            $user->save();
            return redirect('user/profile')->with('successMessage', 'Profile has been updated successfully.');
        } catch (\Exception $e) {
            return back()->withInput()->with('errorMessage', 'Unable to update profile.');
        }

    }

    // api purpose
    public function getAllUsers()
    {
        $users = User::all();
        return response()->json($users);
    }
}
