<?php App::setLocale(Session::get('preferedLocale')); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Favolara</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/themes/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/themes/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/themes/limitless/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="/themes/limitless/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="/themes/limitless/assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="/themes/limitless/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/themes/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/themes/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/themes/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="/themes/limitless/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="/themes/limitless/assets/js/plugins/forms/selects/select2.min.js"></script>

    <script type="text/javascript" src="/themes/limitless/assets/js/core/app.js"></script>
    <script type="text/javascript" src="/themes/limitless/assets/js/pages/datatables_basic.js"></script>
    <!-- /theme JS files -->

    {{-- Touch Icon --}}
    @include('partials.touchicon')

    {{-- Custom Styling or JS from Content Page --}}
    @yield('header_script')
</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.html"><img src="/themes/limitless/assets/images/logo.png" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        {{--<p class="navbar-text">--}}
            {{--<span class="label bg-success">Online</span>--}}
        {{--</p>--}}

        {{--<div class="navbar-left" style="margin-top: 5px">--}}
            {{--<div class="dropdown">--}}
                {{--<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">--}}
                    {{--Change Language--}}
                    {{--<span class="caret"></span>--}}
                {{--</button>--}}
                {{--<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">--}}
                    {{--<li><a href="/setting/language/english">English</a></li>--}}
                    {{--<li><a href="/setting/language/malay">Malay </a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="navbar-right">
            <ul class="nav navbar-nav">


                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
{{--                        <img src="{{ Auth::user()->avatar }}" alt="">--}}
                        <span>{{ Auth::user()->name }}</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="/user/profile"><i class="icon-user-plus"></i> My profile</a></li>
                        {{--<li><a href="#"><span class="badge bg-blue pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>--}}
                        <li><a href="/user/change-password"><i class="icon-cog5"></i> Change Password</a></li>
                        <li class="divider"></li>
                        <li><a href="/logout"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="{{ Auth::user()->avatar }}" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold">{{ Auth::user()->name }}</span>
                                <div class="text-size-mini text-muted text-capitalize">
                                    <i class="icon-user-tie text-size-small"></i> &nbsp;{{ str_replace('_', ' ', Auth::user()->role) }}
                                </div>
                            </div>

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="/user/profile"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            @include('partials.backend_sidebar')
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4>
                            <i class="icon-arrow-left52 position-left"></i>
                            <span class="text-semibold">
                                @yield('mainTitle')
                            </span>
                        </h4>
                    </div>

                    <div class="heading-elements">
                        <div class="heading-btn-group">
                            @yield('topButton')
                            {{--<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>--}}
                            {{--<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>--}}
                            {{--<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>--}}
                        </div>
                    </div>
                </div>

                {{--<div class="breadcrumb-line breadcrumb-line-component">--}}
                    {{--<ul class="breadcrumb">--}}
                        {{--<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>--}}
                        {{--<li><a href="datatable_basic.html">Datatables</a></li>--}}
                        {{--<li class="active">Basic</li>--}}
                    {{--</ul>--}}

                    {{--<ul class="breadcrumb-elements">--}}
                        {{--<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                                {{--<i class="icon-gear position-left"></i>--}}
                                {{--Settings--}}
                                {{--<span class="caret"></span>--}}
                            {{--</a>--}}

                            {{--<ul class="dropdown-menu dropdown-menu-right">--}}
                                {{--<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>--}}
                                {{--<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>--}}
                                {{--<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>--}}
                                {{--<li class="divider"></li>--}}
                                {{--<li><a href="#"><i class="icon-gear"></i> All settings</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">
                {{-- Show validation error if it occurs --}}
                @include('partials.notification')
                @yield('content')

                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; {{ \Carbon\Carbon::now()->year }}. <a href="#">Favolara</a> by <a href="http://favotech.com.my">Favotech</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

@yield('footer_script')
</body>
</html>
