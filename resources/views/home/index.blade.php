@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Memo Terkini
@endsection

@section('topButton')
    {{--<a href="/user/create" class="btn btn-link btn-float has-text">--}}
        {{--<i class="icon-pen text-primary"></i>--}}
        {{--<span>Compose</span>--}}
    {{--</a>--}}
@endsection

@section('content')
    @foreach ($memos as $memo)
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $memo.title }}
            </div>
            <div class="panel-body">
                {{ $memo.body }}
            </div>
        </div>
    @endforeach
@endsection

@section('footer_script')
@endsection