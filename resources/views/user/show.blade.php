@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    User Profile
@endsection

@section('topButton')
    @if (Auth::user()->access_power = 200)
        <a href="/user/edit/{{ $user->id }}" class="btn btn-link btn-float has-text">
            <i class="icon-pencil text-danger"></i>
            <span>Edit</span>
        </a>
    @endif
    <a href="/user/update-profile" class="btn btn-link btn-float has-text">
        <i class="icon-user-check text-primary"></i>
        <span>Update Profile</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ $user->name }}
        </div>
        <div class="panel-body">
            <div class="col-md-3">
                <img src="{{ $user->avatar }}" class="img img-thumbnail img-responsive">
            </div>
            <div class="col-md-9">
                <table class="table table-striped">
                    <tr>
                        <th>Name</th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <th>Phone</th>
                        <td>{{ $user->phone }}</td>
                    </tr>
                    <tr>
                        <th>Role</th>
                        <td class="text-capitalize">{{ str_replace('_', ' ', $user->role) }}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td class="text-capitalize">{{ $user->status }}</td>
                    </tr>
                    <tr>
                        <th>Remark</th>
                        <td>{{ $user->remark }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection