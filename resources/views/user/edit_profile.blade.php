@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Update Profile
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ $user->name }}
        </div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}" required autofocus>

                        @include('partials.error_block', ['item' => 'name'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}" required>

                        @include('partials.error_block', ['item' => 'email'])

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-4 control-label">Phone</label>

                    <div class="col-md-6">
                        <input type="text" value="{{ old('phone', $user->phone) }}" class="form-control" name="phone" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Avatar</label>

                    <div class="col-md-6">
                        <input type="file" class="form-control" name="avatar">
                    </div>

                    @include('partials.error_block', ['item' => 'avatar'])

                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection