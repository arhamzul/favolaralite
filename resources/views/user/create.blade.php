@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    @lang('user.create_main_title')
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('user.create_sub_title')
        </div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>
                    <div class="col-md-6">
                        <input name="name" type="text" class="form-control" value="{{ old('name') }}" required autofocus>
                        @include('partials.error_block', ['item' => 'name'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                    <div class="col-md-6">
                        <input name="email" type="email" class="form-control" value="{{ old('email') }}" required>
                        @include('partials.error_block', ['item' => 'email'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>
                    <div class="col-md-6">
                        <input value="{{ old('password') }}" type="password" class="form-control" name="password" required>
                        @include('partials.error_block', ['item' => 'password'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                    <div class="col-md-6">
                        <input value="{{ old('password_confirmation') }}" type="password" class="form-control" name="password_confirmation" required>
                        @include('partials.error_block', ['item' => 'password_confirmation'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Phone</label>
                    <div class="col-md-6">
                        <input type="text" value="{{ old('phone') }}" class="form-control" name="phone">
                        @include('partials.error_block', ['item' => 'phone'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Avatar</label>
                    <div class="col-md-6">
                        <input type="file" class="form-control" name="avatar" required>
                        @include('partials.error_block', ['item' => 'avatar'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Status</label>
                    <div class="col-md-6">
                        <select name="status" class="form-control" value="{{ old('status') }}">
                            @foreach ($statuses as $status)
                                <option @if (old('status') == $status->key) selected @endif value="{{ $status->key }}">{{ $status->value }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'status'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Role</label>
                    <div class="col-md-6">
                        <select name="role" class="form-control" @if (Auth::user()->role != 'super_admin') disabled @endif>
                            @foreach ($roles as $role)
                                <option @if (old('role') == $role->key) selected @endif value="{{ $role->key }}">{{ title_case($role->value) }}</option>
                            @endforeach
                        </select>
                        @include('partials.error_block', ['item' => 'role'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('remark') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Remark</label>
                    <div class="col-md-6">
                        <textarea name="remark" class="form-control" rows="3"></textarea>
                    </div>
                    @include('partials.error_block', ['item' => 'remark'])
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection