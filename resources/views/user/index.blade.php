@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    All Users
@endsection

@section('topButton')
    <a href="/user/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New User</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Avatar</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>
                            <img class="img-responsive img-thumbnail" style="height: 50px" src="{{ $user->avatar }}">
                        </td>
                        <td><a href="/user/profile/{{ $user->id }}">{{ $user->name }}</a></td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td class="text-capitalize">{{ str_replace('_', ' ', Auth::user()->role) }}</td>
                        @if ($user->status == 'active')
                            <td><span class="label label-success">{{ $user->status }}</span></td>
                        @elseif ($user->status == 'inactive')
                            <td><span class="label label-danger">{{ $user->status }}</span></td>
                        @else
                            <td><span class="label label-default">{{ $user->status }}</span></td>
                        @endif
                            <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="/user/show/{{ $user->id }}"><i class="icon-display"></i> View</a></li>
                                        <li><a href="/user/reset-password/{{ $user->id }}"><i class="icon-lock"></i> Reset Password</a></li>
                                        <li><a href="/user/edit/{{ $user->id }}"><i class="icon-pencil"></i> Edit</a></li>
                                        <li><a href="/user/destroy/{{ $user->id }}"><i class="icon-trash"></i> Delete</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection