@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Change Password
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('user.create_sub_title')
        </div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Old Password</label>

                    <div class="col-md-6">
                        <input value="{{ old('old_password') }}" type="password"
                               class="form-control" name="old_password" required>

                        @include('partials.error_block', ['item' => 'password'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input value="{{ old('password') }}" type="password" class="form-control" name="password"
                               required>

                        @include('partials.error_block', ['item' => 'password'])
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" value="{{ old('password_confirmation') }}" type="password" class="form-control" name="password_confirmation" required>
                        @include('partials.error_block', ['item' => 'password_confirmation'])
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Change Password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection