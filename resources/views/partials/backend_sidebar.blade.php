<!-- User & Above -->
<li class="navigation-header"><span>User</span> <i class="icon-menu" title="Main pages"></i></li>
<li @if (Request::segment(1) == 'home') class="active" @endif><a href="/home"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
{{--<li @if (Request::segment(1) == 'profile') class="active" @endif><a href="/user/profile"><i class="icon-user"></i> <span>Profile</span></a></li>--}}
<!-- /user and above -->

<!-- Admin Only -->
@if (Auth::user()->access_power >= 200)
    <li class="navigation-header"><span>ADMIN</span> <i class="icon-menu" title="Main pages"></i></li>
    <li @if (Request::segment(1) == 'memo') class="active" @endif><a href="/memo"><i class="icon-mention"></i> <span>Memo</span></a></li>

@endif

<!-- /admin only -->

{{-- Super Admin Only --}}
@if (Auth::user()->role == 'super_admin')
    <li class="navigation-header"><span>SUPER ADMIN</span> <i class="icon-menu" title="Main pages"></i></li>
    <li @if (Request::segment(1) == 'user') class="active" @endif>
        <a href="#"><i class="icon-users"></i> <span>User Management</span></a>
        <ul>
            <li><a href="/user/create">Create New User</a></li>
            <li><a href="/user">All Users</a></li>
        </ul>
    </li>
    <li @if (Request::segment(1) == 'audit') class="active" @endif><a href="/audit"><i class="icon-list"></i> <span>Audit Trail</span></a></li>
@endif
{{-- End Super Admin--}}