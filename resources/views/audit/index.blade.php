@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Audit Trails
@endsection

@section('topButton')
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>#</th>
                    <th>User</th>
                    <th>URL</th>
                    <th class="text-center">IP</th>
                    <th class="text-center">Date Time</th>
                    <th class="text-center">Flag</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($audits as $audit)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $audit->user->name }}</td>
                        <td>{{ $audit->url }}</td>
                        <td class="text-center">{{ $audit->ip }}</td>
                        <td class="text-center">{{ $audit->created_at }}</td>
                        <td class="text-center"><span class="label {{ $audit->flag == 'normal'?"label-success":"label-danger" }}">{{ $audit->flag }}</span></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="/audit/mark/{{ $audit->id }}/danger"><i class="icon-warning text-danger"></i> Mark as Dangerous</a></li>
                                        <li><a href="/audit/mark/{{ $audit->id }}/normal"><i class="icon-history"></i> Mark as Normal</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer_script')

@endsection