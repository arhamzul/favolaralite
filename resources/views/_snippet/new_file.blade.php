@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Create New User
@endsection

@section('topButton')
    <a href="/user/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New User</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Sub Title
        </div>
        <div class="panel-body">

        </div>
    </div>
@endsection

@section('footer_script')
@endsection