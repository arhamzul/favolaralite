@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Memo Profile
@endsection

@section('topButton')
    @if (Auth::memo()->access_power = 200)
        <a href="/memo/edit/{{ $memo->id }}" class="btn btn-link btn-float has-text">
            <i class="icon-pencil text-danger"></i>
            <span>Edit</span>
        </a>
    @endif
    <a href="/memo/update-profile" class="btn btn-link btn-float has-text">
        <i class="icon-memo-check text-primary"></i>
        <span>Update Profile</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ $memo->name }}
        </div>
        <div class="panel-body">
            <div class="col-md-3">
                <img src="{{ $memo->avatar }}" class="img img-thumbnail img-responsive">
            </div>
            <div class="col-md-9">
                <table class="table table-striped">
                    <tr>
                        <th>Name</th>
                        <td>{{ $memo->name }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $memo->email }}</td>
                    </tr>
                    <tr>
                        <th>Phone</th>
                        <td>{{ $memo->phone }}</td>
                    </tr>
                    <tr>
                        <th>Role</th>
                        <td class="text-capitalize">{{ str_replace('_', ' ', $memo->role) }}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td class="text-capitalize">{{ $memo->status }}</td>
                    </tr>
                    <tr>
                        <th>Remark</th>
                        <td>{{ $memo->remark }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection