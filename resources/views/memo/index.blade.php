@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    All Memos
@endsection

@section('topButton')
    <a href="/memo/create" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>New Memo</span>
    </a>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Body</th>
                    <th>Created At</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($memos as $memo)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td><a href="/memo/profile/{{ $memo->id }}">{{ $memo->title }}</a></td>
                        <td>{{ $memo->body }}</td>
                        <td>{{ $memo->created_at }}</td>

                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="/memo/show/{{ $memo->id }}"><i class="icon-display"></i> View</a>
                                        </li>
                                        <li><a href="/memo/reset-password/{{ $memo->id }}"><i class="icon-lock"></i>
                                                Reset Password</a></li>
                                        <li><a href="/memo/edit/{{ $memo->id }}"><i class="icon-pencil"></i> Edit</a>
                                        </li>
                                        <li><a href="/memo/destroy/{{ $memo->id }}"><i class="icon-trash"></i>
                                                Delete</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection