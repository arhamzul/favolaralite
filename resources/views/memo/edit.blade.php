@extends('layouts.backend')

@section('header_script')
@endsection

@section('mainTitle')
    Memo
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Kemaskini Memo
        </div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-3 control-label">Title</label>
                    <div class="col-md-7">
                        <input name="name" type="text" class="form-control" value="{{ old('title', $memo->title) }}" required autofocus>
                        @include('partials.error_block', ['item' => 'title'])
                    </div>
                </div>

                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-3 control-label">E-Mail Address</label>
                    <div class="col-md-7">
                        <textarea class="form-control" required name="body" rows="3">{{ old('body', $memo->body) }}</textarea>
                        @include('partials.error_block', ['item' => 'body'])
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-primary">
                            Hantar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_script')
@endsection