<?php

return [
    'create_main_title' => 'Create New User',
    'create_sub_title' => 'New User Form',
    'name' => 'Name',
];